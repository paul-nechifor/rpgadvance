<?php 
require_once 'include/header.php';
print_header('Top Films');
?>

<h2 id="topfilms__title">Top Films</h2>

<div class="writing">
<p>A list of my favorite films of all time. The list is sortable if you have JavaScript enabled. Just click on the table header to sort and click again to reverse order.</p>
</div>

<table id="favfilms" class="sortable" summary="A list of my favorite films of all time.">
<thead>
<tr>
	<th style="width:140px;">Title</th>
	<th style="width:100px;">Director</th>
	<th style="width:40px;">Year</th>
	<th style="width:50px;">IMDB</th>
	<th style="width:90px;">My Grade</th>
	<th style="width:50px;">Diff.</th>
</tr>
</thead>
<tbody>
<tr>
	<td>Pulp Fiction</td>
	<td>Quentin Tarantino</td>
	<td>1994</td>
	<td><a href="http://imdb.com/title/tt0110912/">8,8</a></td>
	<td>9,80</td>
	<td>1,00</td>
</tr>
<tr>
	<td>Taxi Driver</td>
	<td>Martin Scorsese</td>
	<td>1976</td>
	<td><a href="http://imdb.com/title/tt0075314/">8,5</a></td>
	<td>9,75</td>
	<td>1,25</td>
</tr>
<tr>
	<td>Elephant</td>
	<td>Gus Van Sant</td>
	<td>2003</td>
	<td><a href="http://imdb.com/title/tt0363589/">7,3</a></td>
	<td>9,70</td>
	<td>2,40</td>
</tr>
<tr>
	<td>Kill Bill: Vol. 1 &amp; 2</td>
	<td>Quentin Tarantino</td>
	<td>2003</td>
	<td><a href="http://imdb.com/title/tt0266697/%0A%0A">8,3</a></td>
	<td>9,65</td>
	<td>1,35</td>
</tr>
<tr>
	<td>2001: A Space Odyssey</td>
	<td>Stanley Kubrick</td>
	<td>1968</td>
	<td><a href="http://imdb.com/title/tt0062622/">8,3</a></td>
	<td>9,60</td>
	<td>1,30</td>
</tr>
<tr>
	<td>Alexis Zorbas</td>
	<td>Michael Cacoyannis</td>
	<td>1964</td>
	<td><a href="http://imdb.com/title/tt0057831/">7,8</a></td>
	<td>9,55</td>
	<td>1,75</td>
</tr>
<tr>
	<td>Arizona Dream</td>
	<td>Emir Kusturica</td>
	<td>1993</td>
	<td><a href="http://imdb.com/title/tt0106307/">7,1</a></td>
	<td>9,50</td>
	<td>2,40</td>
</tr>
<tr>
	<td>The Return of the King</td>
	<td>Peter Jackson</td>
	<td>2003</td>
	<td><a href="http://imdb.com/title/tt0167260/">8,9</a></td>
	<td>9,45</td>
	<td>0,55</td>
</tr>
<tr>
	<td>Nineteen Eighty-Four</td>
	<td>Michael Radford</td>
	<td>1984</td>
	<td><a href="http://imdb.com/title/tt0087803/">8,0</a></td>
	<td>9,40</td>
	<td>1,40</td>
</tr>
<tr>
	<td>The Two Towers</td>
	<td>Peter Jackson</td>
	<td>2002</td>
	<td><a href="http://imdb.com/title/tt0167261/">8,7</a></td>
	<td>9,35</td>
	<td>0,65</td>
</tr>
<tr>
	<td>The Fellowship of the Ring</td>
	<td>Peter Jackson</td>
	<td>2001</td>
	<td><a href="http://imdb.com/title/tt0120737/">8,8</a></td>
	<td>9,30</td>
	<td>0,50</td>
</tr>
<tr>
	<td>Sleepers</td>
	<td>Barry Levinson</td>
	<td>1996</td>
	<td><a href="http://imdb.com/title/tt0117665/">7,1</a></td>
	<td>9,25</td>
	<td>2,15</td>
</tr>
<tr>
	<td>The Holy Mountain</td>
	<td>Alejandro Jodorowsky</td>
	<td>1973</td>
	<td><a href="http://imdb.com/title/tt0071615/">7,9</a></td>
	<td>9,20</td>
	<td>1,30</td>
</tr>
<tr>
	<td>The Exorcist</td>
	<td>William Friedkin</td>
	<td>1973</td>
	<td><a href="http://imdb.com/title/tt0070047/">8,0</a></td>
	<td>9,15</td>
	<td>1,15</td>
</tr>
<tr>
	<td>Bad Santa</td>
	<td>Terry Zwigoff</td>
	<td>2003</td>
	<td><a href="http://imdb.com/title/tt0307987/">7,2</a></td>
	<td>9,10</td>
	<td>1,90</td>
</tr>
<tr>
	<td>Bang, Bang, You're Dead</td>
	<td>Guy Ferland</td>
	<td>2002</td>
	<td><a href="http://imdb.com/title/tt0288439/">8,3</a></td>
	<td>9,05</td>
	<td>0,75</td>
</tr>
<tr>
	<td>The Pianist</td>
	<td>Roman Polanski</td>
	<td>2002</td>
	<td><a href="http://imdb.com/title/tt0253474/">8,5</a></td>
	<td>9,00</td>
	<td>0,50</td>
</tr>
<tr>

	<td>Begotten</td>
	<td>E. Elias Merhige</td>
	<td>1991</td>
	<td><a href="http://imdb.com/title/tt0101420/">6,0</a></td>
	<td>8,95</td>
	<td>2,95</td>
</tr>
<tr>
	<td>Fight Club</td>
	<td>David Fincher</td>
	<td>1999</td>
	<td><a href="http://imdb.com/title/tt0137523/">8,6</a></td>
	<td>8,90</td>
	<td>0,30</td>
</tr>
<tr>
	<td>Papilon</td>
	<td>Franklin J. Schaffner</td>
	<td>1973</td>
	<td><a href="http://imdb.com/title/tt0070511/">7,9</a></td>
	<td>8,85</td>
	<td>0,95</td>
</tr>
<tr>

	<td>Saw</td>
	<td>James Wan</td>
	<td>2004</td>
	<td><a href="http://imdb.com/title/tt0387564/">7,6</a></td>
	<td>8,80</td>
	<td>1,20</td>
</tr>
<tr>
	<td>Bowling for Columbine</td>
	<td>Michael Moore</td>
	<td>2002</td>
	<td><a href="http://imdb.com/title/tt0310793/">8,3</a></td>
	<td>8,75</td>
	<td>0,45</td>
</tr>
<tr>
	<td>El Topo</td>
	<td>Alejandro Jodorowsky</td>
	<td>1970</td>
	<td><a href="http://imdb.com/title/tt0067866/">7,3</a></td>
	<td>8,70</td>
	<td>1,40</td>
</tr>
<tr>

	<td>The Devil's Rejects</td>
	<td>Rob Zombie</td>
	<td>2005</td>
	<td><a href="http://imdb.com/title/tt0395584/">7,0</a></td>
	<td>8,65</td>
	<td>1,65</td>
</tr>
<tr>
	<td>Il Buono, il brutto, il cattivo</td>
	<td>Sergio Leone</td>
	<td>1966</td>
	<td><a href="http://imdb.com/title/tt0060196/">8,9</a></td>
	<td>8,60</td>
	<td>-0,30</td>
</tr>
<tr>
	<td>Thirteen</td>
	<td>Catherine Hardwicke</td>
	<td>2003</td>
	<td><a href="http://imdb.com/title/tt0328538/">7,1</a></td>
	<td>8,55</td>
	<td>1,45</td>
</tr>
<tr>
	<td>The Butterfly Effect</td>
	<td>Eric Bress</td>
	<td>2004</td>
	<td><a href="http://imdb.com/title/tt0289879/">7,7</a></td>
	<td>8,50</td>
	<td>0,80</td>
</tr>
<tr>
	<td>The Shawshank Redemption</td>
	<td>Frank Darabont</td>
	<td>1994</td>
	<td><a href="http://imdb.com/title/tt0111161/">9,1</a></td>
	<td>8,45</td>
	<td>-0,65</td>
</tr>
<tr>
	<td>Life of Brian</td>
	<td>Terry Jones</td>
	<td>1979</td>
	<td><a href="http://imdb.com/title/tt0079470/">8,1</a></td>
	<td>8,40</td>
	<td>0,30</td>
</tr>
<tr>

	<td>Silence of the Lambs</td>
	<td>Jonathan Demme</td>
	<td>1991</td>
	<td><a href="http://imdb.com/title/tt0102926/">8,6</a></td>
	<td>8,35</td>
	<td>-0,25</td>
</tr>
<tr>
	<td>Mystic River</td>
	<td>Clint Eastwood</td>
	<td>2003</td>
	<td><a href="http://imdb.com/title/tt0327056/">8,0</a></td>
	<td>8,30</td>
	<td>0,30</td>
</tr>
<tr>
	<td>Le fabuleaux destin d'Amelie Poulain</td>
	<td>Jean-Pierre Jeunet</td>
	<td>2001</td>
	<td><a href="http://imdb.com/title/tt0211915/">8,6</a></td>
	<td>8,25</td>
	<td>-0,35</td>
</tr>
<tr>
	<td>Shaun of the Dead</td>
	<td>Edgar Wright</td>
	<td>2004</td>
	<td><a href="http://imdb.com/title/tt0365748/">7,9</a></td>
	<td>8,20</td>
	<td>0,30</td>
</tr>
<tr>
	<td>The Virgin Suecides</td>
	<td>Sofia Coppola</td>
	<td>1999</td>
	<td><a href="http://imdb.com/title/tt0159097/">7,2</a></td>
	<td>8,15</td>
	<td>0,95</td>
</tr>
<tr>
	<td>Top Secret!</td>
	<td>Jim Abrahams</td>
	<td>1984</td>
	<td><a href="http://imdb.com/title/tt0088286/">6,9</a></td>
	<td>8,10</td>
	<td>1,20</td>
</tr>
<tr>
	<td>The Root of all Evil?</td>
	<td>N/A</td>
	<td>2006</td>
	<td><a href="http://imdb.com/title/tt0774118/">8,7</a></td>
	<td>8,05</td>
	<td>-0,65</td>
</tr>
<tr>
	<td>Red Dragon</td>
	<td>Brett Ratner</td>
	<td>2002</td>
	<td><a href="http://imdb.com/title/tt0289765/">7,3</a></td>
	<td>8,00</td>
	<td>0,70</td>
</tr>
</tbody>
</table>

		
<?php
require_once 'include/footer.php';
?>
