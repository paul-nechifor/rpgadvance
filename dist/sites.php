<?php
require_once 'include/header.php';
print_header('Sites',$_GET['style'], '2006-09-08T14:01:00+02:00');
?>
<div id="title">
	<h2 id="sites_title">Sites</h2>
</div>
<div class="writing">
	<p class="introduction">Here I&#8217;ll put some of the sites I made</p>
	<p><a href="index.php">An RPG Advancement</a> (this site) is my first fully <a href="http://en.wikipedia.org/wiki/XHTML#Overview">XHTML 1.0 Strict</a> compliant web site. I&#8217;ve made other sites (not so good HTML 4 with tables) but I want to put my past web design behind me and continue this way. I made more than just one design for this site, all thanks to CSS.</p>
	<p>My <a href="scripts/index.php">PHP Scripts</a> (on this site also) is where I place the scripts that I make.</p>
	<p>If you&#8217;re interested in my designs and want me to make a site for you go to the <a href="contact.php">Contact</a> page. I design sites for free but only if I like what I make. So if you think I&#8217;d be interested in your site and don&#8217;t mind supporting only newer browsers (like <a href="http://www.mozilla.com/firefox/">Firefox</a>, <a href="http://www.apple.com/macosx/features/safari/">Safari</a> (on Mac), <a href="http://www.opera.com/">Opera</a> or <a href="http://en.wikipedia.org/wiki/Internet_Explorer#Version_7">IE 7</a>) <span title="Wow... I got to use this expression">drop me a line</span>.</p>
</div>

<?php
require_once 'include/footer.php';
print_footer($_GET['style']);
?>
