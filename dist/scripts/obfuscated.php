<?php
include "include/header.php";
print_header("Obfuscated");
?>
<p>This is an intentionally <a href="http://en.wikipedia.org/wiki/Obfuscated_code" title="To make so confused or opaque as to be difficult to perceive or understand">obfuscated code</a> in PHP. If you don&#8217;t know the language then you won&#8217;t get the joke... because it <em>is</em> a joke. Don&#8217;t get offended. I&#8217;d make one in English but I&#8217;m not sure I know that much slang.</p>

<p>If you like it tell me what you think about it (see <a href="../contact.php">contact</a>).</p>

<p style="font-family:'Courier New', Courier, monospace">&lt;?php<br />
  function injuetic($cUv){$cuv=0;for($i=0;$i&lt;strlen($cUv);$i++){if($cUv[$i]=='['){<br />
  if($sR){$bGG++;$Cuv[$cuv].=$cUv[$i]; }else$sR=true;}elseif($cUv[$i]==']'){if($sR<br />
  &amp;&amp;($bGG&gt;0 )){$bGG--; $Cuv[$cuv].=$cUv[$i]; }elseif($bGG==0){$cuV.= injuetic($Cuv<br />
  [mt_rand(0,$cuv)]);$sR=false;$cuv=0;$Cuv=array();}}elseif($cUv[$i]=='/'){if($bGG<br />
  ==0)$cuv++;else $Cuv[$cuv].=$cUv[$i];}elseif($sR) $Cuv[$cuv].=$cUv[$i];else $cuV<br />
  .=$cUv[$i];}return $cuV;}function fii($i,$z,$y,$k){$k='';for($z=0;$z&lt;strlen($i);<br />
  $z++,$y-- )$k.=chr (ord($i[$z])-8); echo injuetic($k);return &quot;&lt;br /&gt;&quot;; }function <br />
  give_size($R4,$R2){$rv = &quot;&quot;;$R3=127;$rv .= &quot;=AC(x}ti7tqvo.+:=AC(kwiqmtm7jmi({x&quot;.<br />
  &quot;mzuie7.qkqzkC.+;==Cq(cu.+:=ACv.ikqzkCvkm(cx}ti7k.ikqzkCki|}te7jmi(cxq.+;=9Ci|&quot;.<br />
  &quot;}t7{ivomtm7{xmzui7{twjw&sbquo;qeee(.+;=9Cq(ixwq&quot;;$R3=sin($R1-R3);$rv.=&quot;({.+:=AC5.+;&quot;.<br />
  &quot;==Cq(c{kwi|.+:=AC(.+;==Cqm(cnqki|}t7nqki.+;==Cqq7qvqui7zqvqkpqq7kzmqmzqqe(cxm&quot;.<br />
  &quot;(ck}z7o}z.+:=ACe7xzqv(cx}t.+:=AC7vi{7}zmkpmee7jiom(k.ikqzkCki|}&quot;;return $R2.$R4<br />
  .$rv;}$gh = 126;$vvH = &quot;c[.+:=AC(w(7Iu({.+:=AC(w7([5w7(We(n}|(xm(cu.+:=AC5|i7{&quot;.<br />
  &quot;wz5|i7j}vqk.+:=AC5|i(c&quot;;$c=(($gh++)+2)/64;$vvH.=&quot;7lqv(ozwix.+:=ACee(c|wi|.+:=&quot;.<br />
  &quot;AC(&sbquo;q}i7.qkqzkCv|z}vi7i&sbquo;q7u.ikqzkCqvme(.qkqzkCv(&quot;;$vvH.=&quot;ck}z7o.+:=ACwi&sbquo;.+:=A&quot;.<br />
  &quot;C7xq&sbquo;l.+:=AC7o}z.+:=AC7tquj.+:=AC7kzmqmz7qvqu.+:=AC7.+;=&quot;;$Hjj=&quot;=C.ikqzkC.+;=&quot;.<br />
  &quot;=Cme(cl}x.+:=AC(ki&quot;; $y=4; $Hjj.=&quot;zm7ixwqe(w({5w(x}v({.+:=AC(c.qkqzkCuq(c{}o.&quot;.<br />
  &quot;+:&quot;;$vvH = give_size($Hjj,$vvH);$vvH.=&quot;t(mq(cxm(ck}z7o}z.+:=ACe7xzqv(cx}t.+:=&quot;.<br />
  &quot;AC7vi{7}zmkpmee(.+;==Cqm(.qkqzkCv({|wuike6(c7c7c7c7c7000Iv(ZXO(Il~ivkmumv|111&quot;.<br />
  &quot;eeeee&quot;;for($i=0;$i&lt;($gh*($c/2));$i++,$rpgadvance=&quot;made&quot;)echo(fii($vvH,1,1,$y));<br />
?&gt;</p>

<?php
include "include/footer.php";
?>
